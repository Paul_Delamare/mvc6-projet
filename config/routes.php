<?php

$routes = array(
    array('home','default','index'),

    array('admin','admin','index'),

    array('abonne','abonne','abonne'),
    array('single-abonne','abonne','single', array('id')),
    array('add-abonne','abonne','add_abonne'),
    array('update-abonne','abonne','update', array('id')),
    array('delete-abonne','abonne','delete', array('id')),

    array('product','product','product'),
    array('single-product','product','single',array('id')),
    array('add-product','product','add'),
    array('update-product','product','update', array('id')),
    array('delete-product','product','delete', array('id')),

    array('emprunt','emprunt','emprunt'),
    array('rendu','emprunt','rendu', array('id')),
    array('all-emprunts','emprunt','allEmprunts'),

    array('category', 'category', 'index'),
    array('single-category', 'category', 'single', array('id')),
    array('add-category', 'category', 'add'),
    array('update-category', 'category', 'update', array('id')),
    array('delete-category', 'category', 'delete', array('id')),

    array('api_test', 'api', 'index'),
    array('api_test2', 'api', 'index2', array('id')),
    array('api_recipe', 'api', 'recipes'),
    array('api_add_recipe', 'api', 'add_recipe'),

);









