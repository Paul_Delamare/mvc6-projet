<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\EmpruntModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class AbonneController extends BaseController{
    public function abonne(){
        $abonnes= AbonneModel::all();

        $this->render('app.abonne.abonne', array(
            'abonnes'=>$abonnes,
        ), 'admin');
    }
    public function single($id){
        $abonne= $this->getRecipByIdOr404($id);
        $emprunts=EmpruntModel::empruntById($id, 'id_abonne');
        $this->dump($emprunts);
        $this->render('app.abonne.single_abonne', array(
            'abonne'=>$abonne,
            'emprunts'=>$emprunts,
        ), 'admin');
    }

    public function add_abonne(){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
                AbonneModel::add($post);

                $this->redirect('abonne');
            }
        }
        $form= new Form($error);
        $this->render('app.abonne.add_abonne', array(
            'form'=>$form,
        ), 'admin');
    }
    public function update($id){
        $error=[];
        $abonne=$this->getRecipByIdOr404($id);
        if (!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $v= new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
                AbonneModel::update($id, $post);
                $this->redirect('single-abonne/'.$id);
            }
        }
        $form=new Form($error);
        $this->render('app.abonne.update-abonne', array(
            'abonne'=>$abonne,
            'form'=>$form,
        ), 'admin');
    }
    public function delete($id){
        $this->getRecipByIdOr404($id);
        AbonneModel::delete($id);
        $this->redirect('abonne');
    }
    private function getRecipByIdOr404($id){
        $recette=AbonneModel::findById($id);
        if (empty($recette)){
            $this->Abort404();
        }
        return $recette;
    }
    private function validate($v, $post){
        $error=[];
        $error['nom']=$v->textValid($post['nom'], 'nom', 3, 255 );
        $error['prenom']=$v->textValid($post['prenom'], 'prenom', 3, 255 );
        $error['email']=$v->emailValid($post['email']);
        $error['age']=$v->validAge($post['age']);
        return $error;
    }

}