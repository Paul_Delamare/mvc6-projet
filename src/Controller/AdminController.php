<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\EmpruntModel;
use App\Model\ProductModel;
use Core\Kernel\AbstractController;

class AdminController extends BaseController{

    public function index(){
       $abonne= AbonneModel::count();
       $product= ProductModel::count();
       $allEmprunt=EmpruntModel::count();
       $encours= EmpruntModel::countEmpruntsEnCours();
        $this->render('app.admin.index', array(
            'abonne'=>$abonne,
            'product'=>$product,
            'allEmprunt'=>$allEmprunt,
            'encours'=>$encours,
        ), 'admin');
    }
}