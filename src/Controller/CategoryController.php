<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\CategoryModel;
use App\Model\EmpruntModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class CategoryController extends BaseController{
    public function index(){
        $categorys= CategoryModel::all();

        $this->render('app.category.category', array(
            'categorys'=>$categorys,
        ), 'admin');
    }
    public function single($id){
        $category= $this->getRecipByIdOr404($id);

        $this->render('app.category.single-category', array(
            'category'=>$category,
        ), 'admin');
    }
    public function add(){
        $error=[];

        if (!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $v= new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
                CategoryModel::add($post);

                $this->redirect('category');
            }
        }

        $form=new Form($error);

        $this->render('app.category.add-category', array(
            'form'=>$form,
        ), 'admin');
    }
    public function update($id){
        $category= $this->getRecipByIdOr404($id);
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v= new Validation();
            $error=$this->validate($v, $post);
            $this->dump($_POST);

            if ($v->IsValid($error)){
//                CategoryModel::update($id, $post);
//                $this->redirect('single-category/'.$id);
            }
        }
        $form= new Form($error);
        $this->render('app.category.update-category', array(
            'category'=>$category,
            'form'=>$form,
        ), 'admin');
    }

    public function delete($id){
        $this->getRecipByIdOr404($id);
        CategoryModel::delete($id);
        $this->redirect('category');
    }
    private function getRecipByIdOr404($id){
        $recette=CategoryModel::findById($id);
        if (empty($recette)){
            $this->Abort404();
        }
        return $recette;
    }
    private function validate($v, $post){
        $error=[];
        $error['name']=$v->textValid($post['name'], 'name', 3, 255 );
        $error['description']=$v->textValid($post['description'], 'description', 3, 20000 );
        return $error;
    }
}