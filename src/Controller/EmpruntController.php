<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\EmpruntModel;
use App\Model\ProductModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class EmpruntController extends BaseController{

    public function emprunt(){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v= new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
                EmpruntModel::addEmprunt($post);
                $this->redirect('emprunt');
            }
        }
        $form= new Form($error);
        $noms=AbonneModel::all();
        $titres=ProductModel::all();
        $emprunts= EmpruntModel::empruntsEnCours();
        $this->render('app.emprunts.emprunt', array(
            'emprunts'=>$emprunts,
            'form'=>$form,
            'noms'=>$noms,
            'titres'=>$titres
        ), 'admin');
    }

    public function rendu($id){
        $this->getRecipByIdOr404($id);
        EmpruntModel::renduEmprunt($id);
        $this->redirect('emprunt');
    }

    public function allEmprunts(){
        $emprunts=EmpruntModel::AllBorrowHistory();
        $this->render('app.emprunts.all-emprunts', array(
            'emprunts'=>$emprunts
        ), 'admin');
    }

    private function getRecipByIdOr404($id){
        $recette=EmpruntModel::findById($id);
        if (empty($recette)){
            $this->Abort404();
        }
    return $recette;
    }
    private function validate($v, $post){
        $error=[];
        $error['nom']=$v->existIdAbonne($post['nom']);
        $error['titre']=$v->existIdProduct($post['titre']);
        return $error;
    }
}