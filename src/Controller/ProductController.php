<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\CategoryModel;
use App\Model\CatproModel;
use App\Model\ProductModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class ProductController extends BaseController{
    public function product(){
        $products=ProductModel::all();
//        $this->dump($products);

        $this->render('app.product.product', array(
            'products'=>$products,
        ), 'admin');
    }
    public function single($id){
        $product=CatproModel::findByIdProduct($id);
        $this->dump($product);
//        $this->dump($product);

        $this->render('app.product.single-product', array(
            'product'=>$product,
        ), 'admin');
    }

    public function add(){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
//                ProductModel::add($post);

                $this->redirect('product');
            }
        }
        $form=new Form($error);
        $categorys= CategoryModel::all();
        $this->render('app.product.add-product', array(
            'form'=>$form,
            'categorys'=>$categorys,
        ), 'admin');
    }

    public function update($id){
        $product= $this->getRecipByIdOr404($id);
        $error=[];
        if (!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $v= new Validation();
            $error= $this->validate($v, $post);
            if ($v->IsValid($error)){
                ProductModel::update($id, $post);
                $this->redirect('single-product/'.$id);
            }
        }

        $form= new Form($error);

        $this->render('app.product.update-product', array(
            'form'=>$form,
            'product'=>$product,
        ), 'admin');
    }

    public function delete($id){
        $this->getRecipByIdOr404($id);
        ProductModel::delete($id);
        $this->redirect('product');
    }

    private function getRecipByIdOr404($id){
        $recette=ProductModel::findById($id);
        if (empty($recette)){
            $this->Abort404();
        }
        return $recette;
    }
    private function validate($v, $post){
        $error=[];
        $error['titre']=$v->textValid($post['titre'], 'titre', 3, 255 );
        $error['description']=$v->textValid($post['description'], 'description', 3, 20000 );
        $error['reference']=$v->textValid($post['reference'], 'référence', 5, 50);
        if (!empty($post['category'])){
            $not=[];
            foreach ($post['category'] as $category =>$item){
                array_push($not, $v->existIdCategory($item));
            }
            if (!$v->IsValid($not)){
                $error['category']='Veuillez renseigner une categorie valide*';
            }
        }else{
            $error['category']='Veuillez renseigner une categorie*';
        }
        return $error;
    }
}