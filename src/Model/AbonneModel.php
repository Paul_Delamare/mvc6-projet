<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class AbonneModel extends AbstractModel{
    protected static $table = 'abonnes';
    protected string $nom;
    protected string $prenom;
    protected $email;
    protected int $age;

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
    public static function tableAbo(){
        return self::getTable();
    }

    public static function add($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (nom, prenom, email, age, created_at) VALUES ( ? ,?, ?, ?, NOW())", array($post['nom'], $post['prenom'], $post['email'], $post['age']));
    }
    public static function update($id, $post){

        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET nom = ?, prenom = ?, email = ?, age = ? WHERE id = ? ", array($post['nom'], $post['prenom'], $post['email'], $post['age'], $id));
    }


}