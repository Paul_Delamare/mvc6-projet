<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class CategoryModel extends AbstractModel{
    protected static $table = 'categorys';
    protected string $name;
    protected string $description;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    public static function add($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (name, description, created_at) VALUES ( ? ,?, NOW())", array($post['name'], $post['description']));
    }
    public static function update($id, $post){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET name = ?, description = ? WHERE id = ? ", array($post['name'], $post['description'], $id));
    }

}