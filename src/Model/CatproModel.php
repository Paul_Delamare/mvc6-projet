<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class CatproModel extends AbstractModel{
    protected static $table = 'category_product';


    public static function findByIdProduct($id,$columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT E.*, P.*, C.name  FROM " . self::getTable() . " E INNER JOIN products P ON E.id_product = P.id INNER JOIN categorys C ON E.id_category = C.id WHERE P.$columId = ? ", array($id),get_called_class());
    }


}