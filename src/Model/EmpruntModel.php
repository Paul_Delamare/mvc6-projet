<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class EmpruntModel extends AbstractModel{
    protected static $table = 'borrows';



    public static function empruntsEnCours(){
        return App::getDatabase()->query("SELECT E.*, A.nom, A.prenom, P.titre FROM ".self::getTable() ." E INNER JOIN ".AbonneModel::tableAbo()." A ON E.id_abonne = A.id INNER JOIN ".ProductModel::tableProduct()." P ON E.id_product = P.id WHERE E.date_end IS NULL ORDER BY E.date_start ASC",get_called_class());
    }
    public static function AllBorrowHistory(){
        return App::getDatabase()->query("SELECT E.*, A.nom, A.prenom, P.titre FROM ".self::getTable() ." E INNER JOIN ".AbonneModel::tableAbo()." A ON E.id_abonne = A.id INNER JOIN ".ProductModel::tableProduct()." P ON E.id_product = P.id WHERE E.date_end IS NOT NULL ORDER BY E.date_start DESC",get_called_class());
    }
    public static function renduEmprunt($id){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET date_end = NOW() WHERE id = ? ", array( $id));
    }
    public static function addEmprunt($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (id_abonne, id_product, date_start) VALUES ( ? ,?, NOW())", array($post['nom'], $post['titre']));
    }
    public static function empruntById($id, $columId= 'id_abonne'){
        return App::getDatabase()->prepare("SELECT E.*, P.titre FROM " . self::getTable() . " E INNER JOIN ".ProductModel::tableProduct()." P ON E.id_product = P.id WHERE E.$columId = ? ORDER BY E.date_end ASC", array($id),get_called_class());
    }
    public static function countEmpruntsEnCours(){
        return App::getDatabase()->aggregation("SELECT COUNT(id) FROM " . self::getTable()." WHERE date_end IS NULL");
    }
}