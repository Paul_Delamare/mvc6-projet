<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class ProductModel extends AbstractModel{
    protected static $table = 'products';

    protected string $titre;
    protected string $reference;
    protected string $description;

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
    public static function tableProduct(){

        return self::getTable();
    }

    public static function add($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (titre, reference, description) VALUES ( ? ,?, ?)", array($post['titre'], $post['reference'], $post['description']));
    }
    public static function update($id, $post){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET titre = ?, reference = ?, description = ? WHERE id = ? ", array($post['titre'], $post['reference'], $post['description'], $id));
    }



}