<?php
namespace App\Service;

use App\Model\AbonneModel;
use App\Model\CategoryModel;
use App\Model\ProductModel;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }

    public function validAge($age){
        $error='';
        if (is_int((int)$age)){
            if ($age<0 || $age>130){
                $error='L\'âge renseigné n\'est pas valide*';
            }
        }else{
            $error='L\'âge renseigné n\'est pas valide*';
        }
        return $error;
    }
    public function existIdAbonne($id){
        $error='';
        if (empty(AbonneModel::findById($id))){
            $error= 'Cet abonné n\'existe pas';
        }
        return $error;
    }
    public function existIdProduct($id){
        $error='';
        if (empty(ProductModel::findById($id))){
            $error= 'Ce produit n\'existe pas';
        }
        return $error;
    }
    public function existIdCategory($id){
        $not='';
        if (!empty($id)){
            if (empty(CategoryModel::findById($id))){
                $not='erreur';
            }
        }
        return $not;
    }
}
