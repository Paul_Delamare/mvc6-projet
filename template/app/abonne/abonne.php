<section id="listing_abonne">
    <div class="wrapContent">
        <h1>Voici la liste de vos abonnés</h1>
        <ul>
            <?php
            foreach ($abonnes as $abonne){
                echo '<li><div class="name_listing"><a class="single" href="'.$view->path('single-abonne/'.$abonne->id).'">'.$abonne->nom.' '.$abonne->prenom.'</a></div><div class="more_listing"><a class="update_abonne" href="'.$view->path('update-abonne/'.$abonne->id).'">Modifier l\'utilisateur</a><a class="supprimer_abonne" href="'.$view->path('delete-abonne/'.$abonne->id).'">Supprimer</a></div></li>';
            }
            ?>
        </ul>
        <div class="bloc_add">
            <a class="add_abonne" href="<?php echo $view->path('add-abonne'); ?>">Ajouter un utilisateur</a>
        </div>
    </div>
</section>
