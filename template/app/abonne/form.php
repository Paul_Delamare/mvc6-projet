<form action="" method="post">
    <div class="name">
        <div class="nom">
            <?php echo $form->label('nom'); ?>
            <?php echo $form->input('nom', '', $abonne->nom ?? ''); ?>
            <?php echo $form->error('nom'); ?>
        </div>
        <div class="nom">
            <?php echo $form->label('prenom'); ?>
            <?php echo $form->input('prenom', '', $abonne->prenom ?? ''); ?>
            <?php echo $form->error('prenom'); ?>
        </div>
    </div>
    <div class="emailAge">
        <div class="email">
            <?php echo $form->label('email'); ?>
            <?php echo $form->input('email', 'email', $abonne->email ?? ''); ?>
            <?php echo $form->error('email'); ?>
        </div>
        <div class="email">
            <?php echo $form->label('age'); ?>
            <?php echo $form->input('age', 'number', $abonne->age ?? ''); ?>
            <?php echo $form->error('age'); ?>
        </div>
    </div>
    <div class="submit">
        <?php echo $form->submit(); ?>
    </div>
</form>