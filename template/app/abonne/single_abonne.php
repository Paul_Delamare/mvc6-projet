<section id="single_abonne">
    <div class="wrapContent">
        <h1><?php echo $abonne->nom.' '.$abonne->prenom; ?></h1>
        <h2>Informations supplémentaires :</h2>
        <p>Âge : <?php echo $abonne->age;  ?> ans</p>
        <p>Adresse e-mail : <?php echo $abonne->email; ?></p>

        <table>
            <thead>
            <tr>
                <th class="nom_emprunt">Nom du produit</th>
                <th class="titre_emprunt">Date de l'emprunt</th>
                <th class="">Date du retour</th>
            </tr>
            </thead>
            <caption><h2>Tous les emprunts</h2></caption>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php foreach ($emprunts as $emprunt){
                echo '<tr class="product">';
                echo    '<td><a href="'.$view->path('single-product/'.$emprunt->id_product).'">'.$emprunt->titre.'</a></td>';
                echo    '<td class="modif_product">'.date('d/m/Y', strtotime($emprunt->date_start)).'</td>';
                if (empty($emprunt->date_end)){
                    echo    '<td>'.$emprunt->date_end.'</td>';
                }else{
                    echo    '<td>'.date('d/m/Y', strtotime($emprunt->date_end)).'</td>';
                }
                echo '</tr>';
            }  ?>
            </tbody>
        </table>
    </div>
</section>