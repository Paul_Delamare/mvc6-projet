<section id="category">
    <div class="wrapContent">
        <table>
            <thead>
            <tr>
                <th class="nom_produit">Nom de la catégorie</th>
                <th class="titre_modif">Modifier</th>
                <th class="titre_supprimer">Supprimer</th>
            </tr>
            </thead>
            <caption><h2>Toutes les categories</h2></caption>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php foreach ($categorys as $category){
                echo '<tr class="product">';
                echo    '<td><a href="'.$view->path('single-category/'.$category->id).'">'.$category->name.'</a></td>';
                echo    '<td class="modif_product"><a href="'.$view->path('update-category/'.$category->id).'">Modifier</a> </td>';
                echo    '<td><a class="supprimer_product" href="'.$view->path('delete-category/'.$category->id).'">Supprimer</a></td>';
                echo '</tr>';
            }  ?>
            </tbody>
        </table>
        <div class="add_product">
            <a href="<?php echo $view->path('add-category'); ?>">Ajouter une categorie</a>
        </div>
    </div>
</section>