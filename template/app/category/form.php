<form action="" method="post">
    <div class="titre_product product_form">
        <?php echo $form->label('name'); ?>
        <?php echo $form->input('name', '', $category->name ?? ''); ?>
        <?php echo $form->error('name'); ?>
    </div>
    <div class="description_product product_form">
        <?php echo $form->label('description'); ?>
        <?php echo $form->textarea('description', $category->description ?? ''); ?>
        <?php echo $form->error('description'); ?>
    </div>
    <div class="submit">
        <?php echo $form->submit(); ?>
    </div>
</form>