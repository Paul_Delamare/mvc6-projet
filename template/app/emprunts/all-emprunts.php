<section id="allEmprunts">
    <div class="wrapContent">
        <table>
            <thead>
            <tr>
                <th class="nom_emprunt">Nom de l'abonné</th>
                <th class="titre_emprunt">Nom du produit</th>
                <th class="">Date emprunt</th>
                <th class="date_rendu">Date rendu</th>
            </tr>
            </thead>
            <caption><h2>Tous les emprunts rendus</h2></caption>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php foreach ($emprunts as $emprunt){
                echo '<tr class="product">';
                echo    '<td><a href="'.$view->path('single-abonne/'.$emprunt->id_abonne).'">'.$emprunt->nom.' '.$emprunt->prenom.'</a></td>';
                echo    '<td class="modif_product"><a href="'.$view->path('single-product/'.$emprunt->id_product).'">'.$emprunt->titre.'</a> </td>';
                echo    '<td>'.date('d/m/Y', strtotime($emprunt->date_start)).'</td>';
                echo    '<td class="date_rendu">'.date('d/m/Y', strtotime($emprunt->date_end)).'</td>';
                echo '</tr>';
            }  ?>
            </tbody>
        </table>
    </div>
</section>