<section id="emprunts">
    <div class="wrapContent">
        <h1>Ajouter un emprunt</h1>
        <form action="" method="post">
            <div class="select_nom select">
                <?php echo $form->label('nom'); ?>
                <?php echo $form->selectEntity('nom', $noms, 'nom', $emprunts->id_abonne ?? ''); ?>
                <?php echo $form->error('nom'); ?>
            </div>
            <div class="select_product select">
                <?php echo $form->label('titre'); ?>
                <?php echo $form->selectEntity('titre', $titres, 'titre', $emprunts->id_product ?? ''); ?>
                <?php echo $form->error('titre'); ?>
            </div>
            <div class="submit">
                <?php echo $form->submit(); ?>
            </div>
        </form>
        <table>
            <thead>
            <tr>
                <th class="nom_emprunt">Nom de l'abonné</th>
                <th class="titre_emprunt">Nom du produit</th>
                <th class="titre_supprimer">Produit rendu</th>
            </tr>
            </thead>
            <caption><h2>Tous les emprunts en cours</h2></caption>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php foreach ($emprunts as $emprunt){
                echo '<tr class="product">';
                echo    '<td><a href="'.$view->path('single-abonne/'.$emprunt->id_abonne).'">'.$emprunt->nom.' '.$emprunt->prenom.'</a></td>';
                echo    '<td class="modif_product"><a href="'.$view->path('single-product/'.$emprunt->id_product).'">'.$emprunt->titre.'</a> </td>';
                echo    '<td><a class="supprimer_product" href="'.$view->path('rendu/'.$emprunt->id).'">Rendu</a></td>';
                echo '</tr>';
            }  ?>
            </tbody>
        </table>
        <div class="allEmprunts">
            <a href="<?php echo $view->path('all-emprunts'); ?>">Tous les emprunts rendus</a>
        </div>
    </div>
</section>