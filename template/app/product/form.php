<form action="" method="post">
    <div class="titre_product product_form">
        <?php echo $form->label('titre'); ?>
        <?php echo $form->input('titre', '', $product->titre ?? ''); ?>
        <?php echo $form->error('titre'); ?>
    </div>
    <div class="description_product product_form">
        <?php echo $form->label('description'); ?>
        <?php echo $form->textarea('description', $product->description ?? ''); ?>
        <?php echo $form->error('description'); ?>
    </div>
    <div class="reference_product product_form">
        <?php echo $form->label('reference'); ?>
        <?php echo $form->input('reference', '', $product->reference ?? ''); ?>
        <?php echo $form->error('reference'); ?>
    </div>
    <div class="reference_product product_form">
        <?php echo $form->label('category'); ?>
        <?php echo $form->selectEntity('category', $categorys, 'name', $product->id_product ?? '', true); ?>
        <?php echo $form->error('category'); ?>
    </div>
    <div class="submit">
        <?php echo $form->submit(); ?>
    </div>
</form>