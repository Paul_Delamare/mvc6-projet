<section id="products">
    <div class="wrapContent">
        <table>
            <thead>
            <tr>
                <th class="nom_produit">Nom du produit</th>
                <th class="titre_modif">Modifier</th>
                <th class="titre_supprimer">Supprimer</th>
            </tr>
            </thead>
            <caption><h2>Tous les produits</h2></caption>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php foreach ($products as $product){
                echo '<tr class="product">';
                echo    '<td><a href="'.$view->path('single-product/'.$product->id).'">'.$product->titre.'</a></td>';
                echo    '<td class="modif_product"><a href="'.$view->path('update-product/'.$product->id).'">Modifier</a> </td>';
                echo    '<td><a class="supprimer_product" href="'.$view->path('delete-product/'.$product->id).'">Supprimer</a></td>';
                echo '</tr>';
            }  ?>
            </tbody>
        </table>
        <div class="add_product">
            <a href="<?php echo $view->path('add-product'); ?>">Ajouter un produit</a>
        </div>
    </div>
</section>